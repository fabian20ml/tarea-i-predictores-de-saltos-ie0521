#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <vector>
//#include "./functions2.h"

#define SP 48 // Strongly prefer Pshare = 0
#define WP 49 // weakly prefer Pshare = 1
#define WG 50 // weakly prefer Gshare = 2
#define SG 51 // Strongly prefer Gshare = 3

using namespace std;
// Logica del meta predictor
void metaPredictor(parametros gs,parametros ps,parametros &meta,vector<char> &tcontPred){
//necesita que le pasen los parametros del gshare y el pshare ademas de los suyos propios
  int index = decTobinToDec(meta.BHTSize,meta.direccion); // n bits de PC indexan los contadores
  meta.contador = tcontPred[index]; // accede al contador

  switch (meta.contador) {

    case SP:
      meta.prediction = ps.prediction; //escoge la prediccion del pshare
      if (ps.prediction != gs.prediction) { // casos en que pshare y gshare predicen diferente
        if (ps.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
          tcontPred[index]++; // siguiente estado: WP
        } else if (ps.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
          tcontPred[index]++; // siguiente estado: WP
        } else if (ps.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (ps.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      } else { // casos donde predice igual solo se suma el numero de T o NT correctas o incorrectas
        if (ps.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
        } else if (ps.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
        } else if (ps.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (ps.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      }
      //cout << " | " << 'P' <<  " | " << meta.result << " | " << meta.prediction << " | " << ps.check <<endl;
      break;

    case WP:
      meta.prediction = ps.prediction; //toma la prediccion del Pshare
      if (ps.prediction != gs.prediction) { // si pshare y gshare predicen diferente
        if (ps.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
          tcontPred[index]++; // siguiente estado: WG
        } else if (ps.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
          tcontPred[index]++; // siguiente estado: WG
        } else if (ps.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
          tcontPred[index]--; // siguiente estado: SP
        } else if (ps.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
          tcontPred[index]--; // siguiente estado: SP
        }
      } else { // casos donde predice igual solo se suma el numero de T o NT correctas o incorrectas
        if (ps.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
        } else if (ps.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
        } else if (ps.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (ps.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      }
      //cout << " | " << 'P' <<  " | " << meta.result << " | " << meta.prediction << " | " << ps.check <<endl;
      break;

    case WG:
      meta.prediction = gs.prediction; // toma la prediccion del Gshare
      if (ps.prediction != gs.prediction) { // si pshare y gshare predicen diferente
        if (gs.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
          tcontPred[index]--; // siguiente estado: WP
        } else if (gs.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
          tcontPred[index]--; // siguiente estado: WP
        } else if (gs.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
          tcontPred[index]++; // siguiente estado: SG
        } else if (gs.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
          tcontPred[index]++; // siguiente estado: SG
        }
      } else { // casos donde predice igual solo se suma el numero de T o NT correctas o incorrectas
        if (gs.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
        } else if (gs.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
        } else if (gs.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (gs.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      }
      //cout << " | " << 'G' <<  " | " << meta.result << " | " << meta.prediction << " | " << gs.check <<endl;
      break;

    case SG:
      meta.prediction = gs.prediction; //toma la prediccion del gshare
      if (ps.prediction != gs.prediction) { // si pshare y gshare predicen diferente
        if (gs.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
          tcontPred[index]--; // siguiente estado: WG
        } else if (gs.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
          tcontPred[index]--; // siguiente estado: WG
        } else if (gs.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (gs.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      } else { // casos donde predice igual solo se suma el numero de T o NT correctas o incorrectas
        if (gs.check == "Incorrecto" && meta.result=='T') {
          meta.WrongNT++;
        } else if (gs.check == "Incorrecto" && meta.result=='N') {
          meta.WrongT++;
        } else if (gs.check == "Correcto" && meta.result=='T') {
          meta.RightT++;
        } else if (gs.check == "Correcto" && meta.result=='N') {
          meta.RightNT++;
        }
      }
      //cout << " | " << 'G' <<  " | " << meta.result << " | " << meta.prediction << " | " << gs.check <<endl;
      break;

    default:
      std::cout << "Hubo un error" << endl;
      break;

  }//cierra el switch

}//cierra el metodo metaPredictor
