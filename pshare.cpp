//#include "./pshare.h"

// asignaciones de paramatros y llamada a la logica del predictor pshare
void predictorPshare(int s, int ph, int o){
  parametros pshare; //instancia un struct(ubicado en functions2.h)
  pshare.type = "Pshare";
  pshare.BHTSize = s;
  pshare.regPrivate = ph; // numero de bits de la historia privada de cada salto
  pshare.BHTEntries = pow(2,pshare.BHTSize); // numero de contadores

  vector<char> BHT(pshare.BHTEntries,SN); // arreglo de contadores
  vector<deque<int>> PHT(pshare.BHTEntries,deque<int>(pshare.regPrivate,0)); // PHT
  ofstream myfile;
  pshare.output = o; // si es 1 escribe el archivp

  if (pshare.output ==1) {
    myfile.open("Pshare.txt");
    myfile << "PC          Outcome  Prediction  correct/incorrect \n";
  }

  while (cin >> pshare.direccion >> pshare.result) {
    //cin >> pshare.direccion >> pshare.result;
    pPshare(pshare,BHT,PHT); // llamada a la logica del predictor pshare
    if (pshare.output == 1 && pshare.contadorFile<5000) {
      myfile << pshare.direccion << "  " << pshare.result << "         " << pshare.prediction << "            " << pshare.check << '\n';
      pshare.contadorFile++;
    } else if (pshare.contadorFile == 5000) {
      myfile.close();
    }
  }
  printResults(pshare); // imprime los resultados del predictor
}
