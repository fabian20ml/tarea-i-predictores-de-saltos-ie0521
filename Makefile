all: compile

compile:
	g++ -o branch main.cpp -Wall -pedantic

bimodal:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s 3 -bp 0 -gh 4 -ph 3 -o 1

gshare:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s 4 -bp 2 -gh 3 -ph 3 -o 1

pshare:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s 5 -bp 1 -gh 3 -ph 2 -o 1

meta:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s 3 -bp 3 -gh 4 -ph 3 -o 1

clean:
	rm *.txt
	rm branch
