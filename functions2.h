// Este archivo cuenta con funciones de proposito general
// que no son realmente parte de la logica de los predictores
#include <cmath>
#include <vector>
#include <deque>

using namespace std;

// parametros que utilizan los predictores
struct parametros {
  int BHTSize = 3;
  int regGlobal = 0;
  int regPrivate = 0;
  int output = 0;
  int BHTEntries = 0;
  int contadorFile = 0;
  uint RightT = 0;
  uint WrongNT = 0;
  uint RightNT = 0;
  uint WrongT = 0;
  char contador = '0';
  char result= 'N';
  char prediction = 'N';
  long long direccion = 0;
  string type = "Bimodal";
  string check = "Incorrecto";
};

// Funcion que toma la  direccion, y dependiendo de los bits que entran como parametro
// aplica una mascara para obtener los bits que se usaran para la indexacion de tablas
int decTobinToDec(int nBits,uint number){
  int mask = 0;
  int pastmask = 0;
  for (int i = 0; i < nBits; i++)
  {   
      pastmask = mask;
      mask = 1 + (2*pastmask);
  }
  int result = mask & number;
  return result;
}

// Funcion que toma un arreglo de binarios, y transforma todo el arreglo 
// en un decimal
int binToDec (deque<int> arrayBin){
  int result = 0;
  for(uint i=0; i<arrayBin.size(); i++) {
    result += arrayBin[i]*pow(2,i);
  }
  return result;
}

//funcion para imprimir los resultaos de los predictores
void printResults (parametros predictor){
  float branches = predictor.RightT + predictor.RightNT + predictor.WrongT + predictor.WrongNT;
  float percentage =((predictor.RightT +predictor.RightNT)/branches)*100;
  std::cout << "_________________________________________________________" << endl;
  cout << "Prediction parameters: " << endl;
  std::cout << "_________________________________________________________" << endl;
  cout << "Branch prediction Type:                     " << predictor.type << endl;
  cout << "BHT size (entries):                         " << predictor.BHTSize << endl;
  cout << "Global history register size:               "  << predictor.regGlobal << endl;
  cout << "Private history register size:              " << predictor.regPrivate << endl;
  std::cout << "_________________________________________________________" << endl;
  cout << "Simulation results: " << endl;
  std::cout << "_________________________________________________________" << endl;
  cout << "Number of branches:                         " << branches <<endl;
  cout << "Correct prediction of taken branches:       "<< predictor.RightT << endl;
  cout << "Incorrect prediction of taken branches:     "<< predictor.WrongT << endl;
  cout << "Correct prediction of not taken branches:   "<< predictor.RightNT << endl;
  cout << "Incorrect prediction of not taken branches: "<< predictor.WrongNT << endl;
  cout << "Percentage of correct predictions:          " << percentage << '%'<<endl;
  std::cout << "_________________________________________________________" << endl;

}
