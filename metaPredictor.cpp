#include "./gshare2.h"
#include "./pshare.h"
#include "./metaPredictor.h"

using namespace std;

void PredictorXTorneo (int s, int gh, int ph, int o ) {

//main metaPredictor
  parametros pshare,gshare,metaP; // crea 3 structs de parametros (definido en functions2.h)
  //asignacion de parametros
  metaP.type = "Meta predictor";
  metaP.BHTSize = s;
  gshare.BHTSize = metaP.BHTSize;
  pshare.BHTSize = metaP.BHTSize;
  metaP.regGlobal = gh;
  gshare.regGlobal = metaP.regGlobal;
  metaP.regPrivate = ph;
  pshare.regPrivate = metaP.regPrivate;
  metaP.BHTEntries = pow(2,metaP.BHTSize);
  gshare.BHTEntries = metaP.BHTEntries;
  pshare.BHTEntries = metaP.BHTEntries;
  vector<char> BHTmp(metaP.BHTEntries,SP); // contadores de metaPredictor
  vector<char> BHTgs(gshare.BHTEntries,SN); // contadores para gshare
  vector<char> BHTps(pshare.BHTEntries,SN); // contadores para pshare
  deque<int> GlobalHistory(gshare.regGlobal,0); // registro global
  vector<deque<int>> PHT(pshare.BHTEntries,deque<int>(pshare.regPrivate,0)); //PHT

  ofstream myfile;
  metaP.output = o;

  if (metaP.output ==1) {
    myfile.open("MetaPredictor.txt");
    myfile << "PC          Outcome  Prediction  correct/incorrect \n";
  }



  while (cin >> metaP.direccion>> metaP.result) {
    //cin >> metaP.direccion>> metaP.result;
    gshare.direccion = metaP.direccion;
    pshare.direccion = metaP.direccion;
    gshare.result = metaP.result;
    pshare.result = metaP.result;
    pGshare(gshare,BHTgs,GlobalHistory); //llamada a la logica gshare
    pPshare(pshare,BHTps,PHT); // llamada a la logica pshare
    metaPredictor(gshare,pshare,metaP,BHTmp); // llamada a la logica metapredictor
    if (metaP.output == 1 && metaP.contadorFile<5000) {
      myfile << metaP.direccion << "  " << metaP.result << "         " << metaP.prediction << "            " << metaP.check << '\n';
      metaP.contadorFile++;
    } else if (metaP.contadorFile == 5000) {
      myfile.close();
    }
  }

  printResults(metaP); //imprime los resultados del metapredictor
}
