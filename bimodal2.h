#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <vector>
//#include "./functions2.h"

#define SN 48 // Strong not taken = 0
#define WN 49 // Weakly not taken = 1
#define WT 50 // Weakly taken = 2
#define ST 51 // Strong taken = 3

using namespace std;

// Logica del predictor bimodal
void pBimodal(parametros &bimodal, vector<char> &table){

    // index para accesar el arreglo de contadores
    int index = decTobinToDec(bimodal.BHTSize,bimodal.direccion);
    //obtiene el contador
    bimodal.contador = table[index];

    switch (bimodal.contador) {

      case SN:
        bimodal.prediction = 'N';
        if (bimodal.prediction != bimodal.result) {
          bimodal.check = "Incorrecto";
          bimodal.WrongNT++;  // incorrectas predicciones de NT branches
          table[index]++; // proximo estado: WN
        } else {
          bimodal.check = "Correcto";
          bimodal.RightNT++; // correctas predicciones de NT branches
        }
        break;

      case WN:
        bimodal.prediction = 'N';
        if (bimodal.prediction != bimodal.result) {
          bimodal.check = "Incorrecto";
          bimodal.WrongNT++; // incorrectas predicciones de NT branches
          table[index]++; // proximo estado: WT
        } else {
          bimodal.check = "Correcto";
          bimodal.RightNT++; // correctas predicciones de NT branches
          table[index]--; // proximo estado SN
        }
        break;

      case WT:
        bimodal.prediction = 'T';
        if (bimodal.prediction != bimodal.result) {
          bimodal.check = "Incorrecto";
          bimodal.WrongT++; // incorrectas predicciones de T branches
          table[index]--; // proximo estado: WN
        } else {
          bimodal.check = "Correcto";
          bimodal.RightT++; // correctas predicciones de T branches
          table[index]++; // proximo estado: ST
        }
        break;

      case ST:
        bimodal.prediction = 'T';
        if (bimodal.prediction != bimodal.result) {
          bimodal.check = "Incorrecto";
          bimodal.WrongT++; // incorrectas predicciones de T branches
          table[index]--; // proximo estado: WT
        } else {
          bimodal.check = "Correcto";
          bimodal.RightT++; // correctas predicciones de T branches
        }
        break;

      default:
        std::cout << "Hubo un error." << endl;
        break;

    } //cierra el switch

} // cierra el metodo bimodal*/
