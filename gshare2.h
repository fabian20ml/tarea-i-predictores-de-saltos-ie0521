#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <vector>
#include <deque>
#include "./functions2.h"

#define SN 48 // Strong not taken = 0
#define WN 49 // Weakly not taken = 1
#define WT 50 // Weakly taken = 2
#define ST 51 // Weakly taken = 3

using namespace std;

//el metodo gshare recibe como parametros un struct de parametros, su array de contadores
// y su registro gloabal respectivamente
void pGshare(parametros &global, vector<char> &table, deque<int> &GlobalHistory){

    int index1 = decTobinToDec(global.BHTSize,global.direccion); // n bits de la direccion
    int index2 = binToDec(GlobalHistory); // numero que representa el patron en la historia global
    int index3 = index1^index2; // XOR de las  cantidades anteriores
    index3 = index3 & (global.BHTEntries-1); // mascara AND necesaria por si el parametro -gh es mayor a -s
    global.contador = table[index3]; //accede al contador

    // la explicacion del switch se encuentra en bimodal2.h
    switch (global.contador) {

      case SN:
        global.prediction = 'N';
        if (global.prediction != global.result) {
          global.check = "Incorrecto";
          global.WrongNT++;
          table[index3]++;
        } else {
          global.check = "Correcto";
          global.RightNT++;
        }
        break;

      case WN:
        global.prediction = 'N';
        if (global.prediction != global.result) {
          global.check = "Incorrecto";
          global.WrongNT++;
          table[index3]++;
        } else {
          global.check = "Correcto";
          global.RightNT++;
          table[index3]--;
        }
        break;

      case WT:
        global.prediction = 'T';
        if (global.prediction != global.result) {
          global.check = "Incorrecto";
          global.WrongT++;
          table[index3]--;
        } else {
          global.check = "Correcto";
          global.RightT++;
          table[index3]++;
        }
        break;

      case ST:
        global.prediction = 'T';
        if (global.prediction != global.result) {
          global.check = "Incorrecto";
          global.WrongT++;
          table[index3]--;
        } else {
          global.check = "Correcto";
          global.RightT++;
        }
        break;

      default:
        std::cout << "Hubo un error." << endl;
        break;

      } //cierra el switch

    // algoritmo para actualizar el registro global
    if (global.result == 'T') {
      GlobalHistory.push_front(1);
    } else if (global.result == 'N') {
      GlobalHistory.push_front(0);
    }
    GlobalHistory.pop_back();

}// cierra el metodo Gshared
