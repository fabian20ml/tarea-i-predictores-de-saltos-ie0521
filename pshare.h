#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <vector>
#include <deque>
//#include "./functions2.h"

#define SN 48 // Strong not taken = 0
#define WN 49 // Weakly not taken = 1
#define WT 50 // Weakly taken = 2
#define ST 51 // Weakly taken = 3

using namespace std;

// el metodo pshare recibe como parametros un struct de parametros, su array de contadores
// y una tabla PHT respectivamente
void pPshare(parametros &prvt, vector<char> &table, vector<deque<int>> &prvtHistory){

    int index1 = decTobinToDec(prvt.BHTSize,prvt.direccion); // n bits de la direccion
    int index2 = binToDec(prvtHistory[index1]); //numero que representa el patron en la entrada del array PHT
    int index3 = index1^index2; // XOR de las 2 cantidades anteriores
    prvt.contador = table[index3]; // accede al contador

    //explicacion de los cases en bimodal2.h
    switch (prvt.contador) {

      case SN:
        prvt.prediction = 'N';
        if (prvt.prediction != prvt.result) {
          prvt.check = "Incorrecto";
          prvt.WrongNT++;
          table[index3]++;
        } else {
          prvt.check = "Correcto";
          prvt.RightNT++;
        }
        break;

      case WN:
        prvt.prediction = 'N';
        if (prvt.prediction != prvt.result) {
          prvt.check = "Incorrecto";
          prvt.WrongNT++;
          table[index3]++;
        } else {
          prvt.check = "Correcto";
          prvt.RightNT++;
          table[index3]--;
        }
        break;

      case WT:
        prvt.prediction = 'T';
        if (prvt.prediction != prvt.result) {
          prvt.check = "Incorrecto";
          prvt.WrongT++;
          table[index3]--;
        } else {
          prvt.check = "Correcto";
          prvt.RightT++;
          table[index3]++;
        }
        break;

      case ST:
        prvt.prediction = 'T';
        if (prvt.prediction != prvt.result) {
          prvt.check = "Incorrecto";
          prvt.WrongT++;
          table[index3]--;
        } else {
          prvt.check = "Correcto";
          prvt.RightT++;
        }
        break;

      default:
        std::cout << "Hubo un error." << endl;
        break;

      } //cierra el switch

      // algoritmo para actualizar las entradas del PHT
    if (prvt.result == 'T') {
      prvtHistory[index1].push_front(1);
    } else if (prvt.result == 'N') {
      prvtHistory[index1].push_front(0);
    }
    prvtHistory[index1].pop_back();

}// cierra el metodo Gshared
