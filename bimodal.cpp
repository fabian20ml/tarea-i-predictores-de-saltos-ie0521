#include "./bimodal2.h"

using namespace std;

 // asignaciones de paramatros y llamada a la logica del predictor bimodal
void predictorBimodal(int s,int o){
  parametros bimodal; //instancia un struct(ubicado en functions2.h)
  bimodal.BHTSize = s;
  bimodal.BHTEntries = pow(2,bimodal.BHTSize); // cantidad de contadores
  vector<char> BHT(bimodal.BHTEntries,SN); // array de contadores
  ofstream myfile;
  bimodal.output = o; // si es 1 escribe el archivo

  if (bimodal.output ==1) {
    myfile.open("Bimodal.txt");
    myfile << "PC          Outcome  Prediction  correct/incorrect \n";
  }

  while (cin >> bimodal.direccion >> bimodal.result) {
    pBimodal(bimodal,BHT); // llamado a la logica del predictor bimodal
    if (bimodal.output == 1 && bimodal.contadorFile<5000) {
      myfile << bimodal.direccion << "  " << bimodal.result << "         " << bimodal.prediction << "            " << bimodal.check << '\n';
      bimodal.contadorFile++;
    } else if (bimodal.contadorFile == 5000) {
      myfile.close();
    }
  }
  printResults(bimodal); // imprime los resultados del predictor bimodal
}
