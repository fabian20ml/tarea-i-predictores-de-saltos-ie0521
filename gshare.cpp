//#include "./gshare2.h"

using namespace std;

// asignaciones de paramatros y llamada a la logica del predictor gshare
void predictorGshare(int s,int gh,int o){
  parametros gshare; //instancia un struct(ubicado en functions2.h)
  gshare.type = "Gshare";
  gshare.BHTSize = s;
  gshare.regGlobal = gh; // numero de bits del registro global
  gshare.BHTEntries = pow(2,gshare.BHTSize); //numero de contadores

  vector<char> BHT(gshare.BHTEntries,SN); // arreglo de contadores
  deque<int> GlobalHistory(gshare.regGlobal,0); // historia de los n saltos globales
  ofstream myfile;
  gshare.output = o; // si es 1 escribe el archivo

  if (gshare.output ==1) {
    myfile.open("Gshare.txt");
    myfile << "PC          Outcome  Prediction  correct/incorrect \n";
  }

  while (cin >> gshare.direccion >> gshare.result) {
    //cin >> gshare.direccion >> gshare.result;
    pGshare(gshare,BHT,GlobalHistory); // llamado a la logica gshare
    if (gshare.output == 1 && gshare.contadorFile<5000) {
      myfile << gshare.direccion << "  " << gshare.result << "         " << gshare.prediction << "            " << gshare.check << '\n';
      gshare.contadorFile++;
    } else if (gshare.contadorFile == 5000) {
      myfile.close();
    }
  }
  printResults(gshare); // imprime los resultados del predictor
}
