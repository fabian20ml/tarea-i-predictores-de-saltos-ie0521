#include "./metaPredictor.cpp"
#include "./bimodal.cpp"
#include "./gshare.cpp"
#include "./pshare.cpp"

#define bimodal 0
#define pshare 1 
#define gshare 2
#define meta 3

using namespace std;

int main(int argc, char const *argv[]) {
  // variables donde se guardaran temporalmente los parametros de la consola
  int temp_output;
  int temp_predictor;
  int temp_s;
  int temp_gh;
  int temp_ph;
 //algoritmo para tomar parametros de la consola
  for (int i = 0; i < argc; i++){
    if (argv[i][0] == '-' && argv[i][1] == 's'){
      temp_s = atoi(argv[i+1]);
    }
    if (argv[i][0] == '-' && argv[i][1] == 'b' && argv[i][2] == 'p'){
      temp_predictor = atoi(argv[i+1]);
    }
    if (argv[i][0] == '-' && argv[i][1] == 'g' && argv[i][2] == 'h'){
      temp_gh = atoi(argv[i+1]);
    }
    if (argv[i][0] == '-' && argv[i][1] == 'p' && argv[i][2] == 'h'){
      temp_ph = atoi(argv[i+1]);
    }
    if (argv[i][0] == '-' && argv[i][1] == 'o'){
      temp_output = atoi(argv[i+1]);
    }
  }

  switch (temp_predictor) {
    case bimodal: // predictor Bimodal
      predictorBimodal(temp_s,temp_output);
      break;

    case pshare: // Predictor Pshare
      predictorPshare(temp_s,temp_ph,temp_output);
      break;

    case gshare: // predictor gshare
      predictorGshare(temp_s,temp_gh,temp_output);
      break;

    case meta: //meta predictor
      PredictorXTorneo(temp_s,temp_gh,temp_ph,temp_output);
      break;

    default:
      std::cout << "Parametro -bp no valido." << endl;
      break;
  }

  return 0;
}
